from flask import Flask, render_template, request, escape
import re

app = Flask(__name__,static_folder='static')

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/test_password", methods=['POST'])
def test_password():
    username = request.form.get('username')
    password = request.form.get('password')
    testpassword = re.compile(username)
    result = testpassword.match(password)
    if result is None:
        return render_template("home.html", result = "Goed wachtwoord")
    else:
        return render_template("home.html", result = "Gebruik je username niet in je password!")


@app.route("/escape_test_password", methods=['POST'])
def timeout_test_password():
    username = request.form.get('username')
    password = request.form.get('password')
    username = re.escape(username)
    testpassword = re.compile(username)
    result = testpassword.match(password)
    if result is None:
        return render_template("home.html", result = "Goed wachtwoord")
    else:
        return render_template("home.html", result = "Gebruik je username niet in je password!")


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')